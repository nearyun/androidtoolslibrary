package com.tornado.util;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;

public class ScreenManager {

	/**
	 * 设置屏幕不能锁屏
	 * 
	 * @param activity
	 */
	public static final void setUnlocked(Activity activity) {
		Window win = activity.getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		winParams.flags |= (WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
				| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		win.setAttributes(winParams);
	}

	/**
	 * 设置屏幕可以锁屏，当然默认是也是可以锁屏的
	 * 
	 * @param activity
	 */
	public static final void setLocked(Activity activity) {
		Window win = activity.getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		winParams.flags &= (~WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
				& ~WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				& ~WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON & ~WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		win.setAttributes(winParams);
	}
}
