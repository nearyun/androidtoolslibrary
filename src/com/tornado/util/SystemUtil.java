package com.tornado.util;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.TrafficStats;
import android.os.Environment;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class SystemUtil {
	/**
	 * 返回当前程序版本代码,如:1
	 * 
	 * @param context
	 * @return 当前程序版本代码
	 */
	public static int getAppVersionCode(Context context) {
		int versionCode = -1;
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			versionCode = pi.versionCode;
		} catch (Exception e) {
		}
		return versionCode;
	}

	/**
	 * 返回当前程序版本名,如:1.0.1
	 * 
	 * @param context
	 * @return 当前程序版本名
	 */
	public static String getAppVersionName(Context context) {
		String versionName = "";
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			versionName = pi.versionName;
		} catch (Exception e) {
		}
		return versionName;
	}

	/**
	 * 隐藏键盘
	 * 
	 * @param mAct
	 * @param event
	 */
	public static void hideSoftInput(Activity mAct, MotionEvent event) {
		InputMethodManager manager = (InputMethodManager) mAct.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			View focusView = mAct.getCurrentFocus();
			if (focusView != null && focusView.getWindowToken() != null) {
				manager.hideSoftInputFromWindow(focusView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
	}

	/**
	 * 隐藏键盘
	 * 
	 * @param mAct
	 */
	public static void hideSoftInput(Activity mAct) {
		InputMethodManager manager = (InputMethodManager) mAct.getSystemService(Context.INPUT_METHOD_SERVICE);
		View focusView = mAct.getCurrentFocus();
		if (focusView != null && focusView.getWindowToken() != null) {
			manager.hideSoftInputFromWindow(focusView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	/**
	 * SD是否插入或有没有读写能力
	 * 
	 * @return
	 */
	public static boolean isSDMounted() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}

	/**
	 * 获取window窗口大小
	 * 
	 * @param context
	 * @return
	 */
	public static Point getWindowSize(Context context) {
		WindowManager winManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Point sizePoint = new Point();
		winManager.getDefaultDisplay().getSize(sizePoint);
		return sizePoint;
	}

	/**
	 * 震动模式
	 * 
	 * @param context
	 */
	public static void vibrate(Context context) {
		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(new long[] { 500, 600, 500, 600 }, -1);
	}

	/**
	 * 
	 * @param context
	 * @return totalBytes[0]代表上传的流量byte；totalBytes[1]代表下载的流量byte
	 */
	public static long[] getTxRxBytes(Context context) {
		long[] totalBytes = new long[2];
		PackageManager pm = context.getPackageManager();
		List<ApplicationInfo> appliactaionInfos = pm.getInstalledApplications(0);
		for (ApplicationInfo applicationInfo : appliactaionInfos) {
			if (TextUtils.equals(context.getPackageName(), applicationInfo.packageName)) {
				int uid = applicationInfo.uid; // 获得软件uid
				totalBytes[0] = TrafficStats.getUidTxBytes(uid);// 上传的流量byte
				totalBytes[1] = TrafficStats.getUidRxBytes(uid);// 下载的流量 byte
				// 方法返回值 -1 代表的是应用程序没有产生流量 或者操作系统不支持流量统计
			}

		}
		return totalBytes;
		// TrafficStats.getMobileTxBytes();// 获取手机3g/2g网络上传的总流量
		// TrafficStats.getMobileRxBytes();// 手机2g/3g下载的总流量
		//
		// TrafficStats.getTotalTxBytes();// 手机全部网络接口 包括wifi，3g、2g上传的总流量
		// TrafficStats.getTotalRxBytes();// 手机全部网络接口 包括wifi，3g、2g下载的总流量
	}

}
