package com.tornado.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

public class FileIOUtil {
	/**
	 * 获取文件大小
	 * 
	 * @param file
	 * @return 若参数file不存在，或者不是文件，返回0
	 */
	public static long getFileSize(File file) {
		if (file != null && file.exists() && file.isFile()) {
			return file.length();
		} else {
			return 0;
		}
	}

	public static String getFileSizeStringFormat(long size) {
		double kiloByte = size / 1024;
		// if (kiloByte < 1) {
		// return size + " Byte(s)";
		// }

		double megaByte = kiloByte / 1024;
		if (megaByte < 1) {
			BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
			return result1.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " KB";
		}

		double gigaByte = megaByte / 1024;
		if (gigaByte < 1) {
			BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
			return result2.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " MB";
		}

		double teraBytes = gigaByte / 1024;
		if (teraBytes < 1) {
			BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
			return result3.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " GB";
		}
		BigDecimal result4 = new BigDecimal(teraBytes);
		return result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " TB";
	}

	public static long getDirectorySize(File dir) {
		if (dir == null || !dir.exists())
			return 0L;

		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			if (files == null || files.length == 0)
				return 0L;
			long total = 0L;
			for (int i = 0; i < files.length; i++) {
				total += getDirectorySize(files[i]);
			}
			return total;
		} else
			return getFileSize(dir);

	}

	/**
	 * 获取文件大小，返回单位字符
	 * 
	 * @param file
	 * @return
	 */
	public static String getFileSizeStringFormat(File file) {
		long size = getFileSize(file);
		return getFileSizeStringFormat(size);
	}

	/**
	 * 创建目录
	 * 
	 * @param directory
	 * @return
	 */
	public static File mkdirs(String directory) {
		File dir = new File(directory);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	/**
	 * 隐藏媒体文件(在指定目录下生成.nomedia)，如：图片，在图库中看不到
	 */
	public static void hideMediaFile(String directory) {
		createNewFile(directory, ".nomedia");
	}

	/**
	 * 生成指定文件名的文件
	 * 
	 * @param directory
	 * @param filename
	 */
	public static File createNewFile(String directory, String filename) {
		try {
			File dir = mkdirs(directory);
			File file = new File(dir, filename);
			if (!file.exists()) {
				file.createNewFile();
			}
			return file;
		} catch (Exception e) {
			XHLogUtil.printStackTrace(e);
			return null;
		}
	}

	/**
	 * 复制单个文件
	 * 
	 * @param oldPath
	 *            String 原文件路径 如：c:/fqf.txt
	 * @param newPath
	 *            String 复制后路径 如：f:/fqf.txt
	 * @return boolean
	 */
	public static void copyFile(String oldPath, String newPath) {
		try {
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(oldPath);
			if (oldfile.exists()) { // 文件存在时
				InputStream inStream = new FileInputStream(oldPath); // 读入原文件
				FileOutputStream fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[2048];
				while ((byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread; // 字节数 文件大小
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
				fs.close();
			}
		} catch (Exception e) {
			XHLogUtil.printStackTrace(e);
		}

	}

	/**
	 * 复制整个文件夹内容
	 * 
	 * @param oldPath
	 *            String 原文件路径 如：c:/fqf
	 * @param newPath
	 *            String 复制后路径 如：f:/fqf/ff
	 * @return boolean
	 */
	public void copyFolder(String oldPath, String newPath) {

		try {
			(new File(newPath)).mkdirs(); // 如果文件夹不存在 则建立新文件夹
			File a = new File(oldPath);
			String[] file = a.list();
			File temp = null;
			for (int i = 0; i < file.length; i++) {
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}

				if (temp.isFile()) {
					FileInputStream input = new FileInputStream(temp);
					FileOutputStream output = new FileOutputStream(newPath + "/" + (temp.getName()).toString());
					byte[] b = new byte[1024 * 5];
					int len;
					while ((len = input.read(b)) != -1) {
						output.write(b, 0, len);
					}
					output.flush();
					output.close();
					input.close();
				}
				if (temp.isDirectory()) {// 如果是子文件夹
					copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
				}
			}
		} catch (Exception e) {
			XHLogUtil.printStackTrace(e);
		}

	}

	/**
	 * byte[] 写入文件
	 * 
	 * @param bfile
	 * @param filePath
	 * @param fileName
	 */
	public static boolean saveBytesToFile(byte[] bfile, String filePath, String fileName) {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		File file = null;
		try {
			File dir = new File(filePath);
			if (!dir.exists() && dir.isDirectory()) {// 判断文件目录是否存在
				dir.mkdirs();
			}
			file = new File(filePath, fileName);
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			bos.write(bfile);
			return true;
		} catch (Exception e) {
			XHLogUtil.printStackTrace(e);
			return false;
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					XHLogUtil.printStackTrace(e);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					XHLogUtil.printStackTrace(e);
				}
			}
		}
	}
}
