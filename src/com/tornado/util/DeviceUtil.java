package com.tornado.util;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

public class DeviceUtil {
	
	private static String deviceUUID = null;
	private static final String FILE_NAME = "device.io";
	private static final String KEY_UUID = "uuid";

	private static void saveDeviceUUID(Context context, String uuid) {
		if (TextUtils.isEmpty(uuid))
			return;
		SharedPreferences pref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
		pref.edit().putString(KEY_UUID, uuid).commit();
	}

	private static String readDeviceUUID(Context context) {
		SharedPreferences pref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
		return pref.getString(KEY_UUID, null);
	}

	/**
	 * 获取设备uuid
	 * 
	 * @param context
	 * @return
	 */
	public static final String getDeviceUUID(Context context) {
		if (!TextUtils.isEmpty(deviceUUID))
			return deviceUUID;

		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String tmDeviceId = tm.getDeviceId();

		if (TextUtils.isEmpty(tmDeviceId)) {
			tmDeviceId = readDeviceUUID(context);
			if (TextUtils.isEmpty(tmDeviceId)) {
				tmDeviceId = UUID.randomUUID().toString();
				saveDeviceUUID(context, tmDeviceId);
			}
			deviceUUID = tmDeviceId;
		} else {
			UUID duuid = UUID.nameUUIDFromBytes(tmDeviceId.getBytes());
			deviceUUID = duuid.toString();
		}
		return deviceUUID;
	}
}
