package com.tornado.util;

import android.content.Context;
import android.util.Log;

import com.tornado.log4android.LogManager;

public class XHLogUtil {
	private static final String TAG = XHLogUtil.class.getSimpleName();
	private static boolean DEBUG = true;

	public static final void setLogDebug(Context context, boolean flag) {
		LogManager.init(context);
		DEBUG = flag;
	}

	private static String checkStringContent(String msg) {
		return msg == null ? "log --> NullpointException" : msg;
	}

	/**
	 * 打印指定内容信息
	 * 
	 * @param msg
	 */
	public static void printf(String msg) {
		LogManager.i(TAG, msg);
		if (DEBUG)
			System.out.println(checkStringContent(msg));
	}

	/**
	 * 打印指定异常信息
	 * 
	 * @param e
	 */
	public static void printStackTrace(Exception e) {
		LogManager.e(TAG, e);
		if (DEBUG && e != null)
			e.printStackTrace();
	}

	/**
	 * 输出颜色是蓝色的，仅输出debug调试的意思，但它会输出上层的信息，过滤起来可以通过DDMS的Logcat标签来选择.
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void d(String tag, String msg) {
		LogManager.d(tag, msg);
		if (DEBUG)
			Log.d(tag, checkStringContent(msg));
	}

	/**
	 * 输出颜色是红色的，可以想到error错误，这里仅显示红色的错误信息，这些错误就需要我们认真的分析，查看栈的信息了。
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void e(String tag, String msg) {
		LogManager.e(tag, msg);
		if (DEBUG)
			Log.e(tag, checkStringContent(msg));
	}

	/**
	 * 输出为绿色，一般提示性的消息information，它不会输出Log.v和Log.d的信息，但会显示i、w和e的信息
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void i(String tag, String msg) {
		LogManager.i(tag, msg);
		if (DEBUG)
			Log.i(tag, checkStringContent(msg));
	}

	/**
	 * 调试颜色为黑色的，任何消息都会输出，这里的v代表verbose啰嗦的意思
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void v(String tag, String msg) {
		LogManager.v(tag, msg);
		if (DEBUG)
			Log.v(tag, checkStringContent(msg));
	}

	/**
	 * 意思为橙色，可以看作为warning警告，一般需要我们注意优化Android代码，同时选择它后还会输出Log.e的信息。
	 * 
	 * @param tag
	 *            Used to identify the source of a log message. It usually
	 *            identifies the class or activity where the log call occurs.
	 * @param msg
	 *            The message you would like logged.
	 */
	public static void w(String tag, String msg) {
		LogManager.w(tag, msg);
		if (DEBUG)
			Log.w(tag, checkStringContent(msg));
	}

}
