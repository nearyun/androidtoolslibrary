package com.tornado.util;

import android.app.Service;
import android.content.Context;
import android.telephony.TelephonyManager;

public class PhoneUtil {
	public static final boolean isTelPhoneBusy(Context context) {
		TelephonyManager telManager = (TelephonyManager) context.getSystemService(Service.TELEPHONY_SERVICE);
		return TelephonyManager.CALL_STATE_IDLE != telManager.getCallState();
	}
}
