package com.tornado.log4android.factory;

import com.tornado.log4android.appender.Appender;
import com.tornado.log4android.appender.LogCatAppender;
import com.tornado.log4android.format.PatternFormatter;

public enum DefaultAppenderFactory {
	;
	
	public static Appender createDefaultAppender() {
		Appender appender = new LogCatAppender();
		appender.setFormatter(new PatternFormatter());
		
		return appender;
	}
}
