package com.tornado.log4android.factory;

import com.tornado.log4android.repository.DefaultLoggerRepository;
import com.tornado.log4android.repository.LoggerRepository;

public enum DefaultRepositoryFactory {
	;
	
	public static LoggerRepository getDefaultLoggerRepository() {
		return DefaultLoggerRepository.INSTANCE;
	}
}
